import axios from "axios";

const TOKEN =
    "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdF9uYW1lIjoiQWRtaW4iLCJsYXN0X25hbWUiOiJVc2VyIiwiZW1haWwiOiJhZG1pbkB5b3BtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIn0sImlhdCI6MTY5MDIwMDI2OCwiZXhwIjoxNzIxNzU3ODY4fQ.vPiZifNO34I0YQMOcvBi-A3hw6jHDJ3B8cuEPgUpvCM";
const baseURL = "https://apidev.gavaria.com/api/";
export const podcastConfig = axios.create({
    baseURL: "https://apidev.gavaria.com/api/",
    headers: {
        "Content-Type": "application/json",
        Authorization: `Bearer ${TOKEN}`,
    },
});

export async function fetchData(data = "", isMeta, sectionsDetails) {
    if (sectionsDetails) {
        try {
            if (!sectionsDetails)
                throw new Error("sectionsDetails is not defined");

            const headers = {};
            if (TOKEN) {
                headers["Authorization"] = `Bearer ${TOKEN}`;
            }

            const params = {
                limit: sectionsDetails?.apiUrl?.limit,
                page: sectionsDetails?.apiUrl?.page,
            };

            const isCollection =
                sectionsDetails.isCollection?.toLowerCase() === "yes";
            let url = null;
            if (isMeta) {
                url = isCollection
                    ? baseURL + sectionsDetails.apiUrl
                    : baseURL + sectionsDetails.apiUrl.url;
            } else {
                url = isCollection
                    ? baseURL +
                      sectionsDetails.apiUrl +
                      sectionsDetails.item?.id
                    : baseURL + sectionsDetails.apiUrl.url;
            }

            if (isMeta) {
                const requests =
                    isCollection && Array.isArray(sectionsDetails.item)
                        ? sectionsDetails.item?.map((item) =>
                              axios.request({
                                  method: item?.method || method,
                                  url: url + item?.id,
                                  headers: headers,
                                  params: params,
                              })
                          )
                        : [
                              axios.request({
                                  method:
                                      sectionsDetails.apiUrl.method || method,
                                  url: url,
                                  headers: headers,
                                  params: params,
                              }),
                          ];

                const responses = await Promise.all(requests);

                const newCollections = responses.flatMap(
                    (response) => response.data.collection
                );
                const newCards = responses.flatMap(
                    (response) => response.data.items
                );

                const docs = responses.flatMap(
                    (response) => response.data.docs
                );
                if (isMeta && !isCollection) {
                    return { docs: docs };
                } else {
                    return { collections: newCollections, cards: newCards };
                }
            } else {
                const response = await axios({
                    method:
                        sectionsDetails?.item?.method ||
                        sectionsDetails?.apiUrl?.method,
                    url: url,
                    data: data,
                    headers: headers,
                    params: params,
                });
                if (response.status >= 200 && response.status < 300) {
                    if (isCollection) {
                        return response.data.items;
                    } else {
                        return response.data.docs;
                    }
                } else {
                    throw new Error(response.statusText);
                }
            }
        } catch (error) {
            console.error(error);
            throw error;
        }
    }
}
