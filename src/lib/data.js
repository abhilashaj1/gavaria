export const data = [
    { id: 1, play: true, plus: true },
    { id: 2 },
    { id: 3, play: true },
    { id: 4 },
    { id: 5 },
    { id: 6 },
    { id: 7 },
    { id: 8 },
    { id: 9 },
    { id: 10 },
    { id: 11 },
    { id: 12 },
    { id: 13 },
    { id: 14 },
    { id: 15 },
    { id: 16 },
    { id: 17 },
    { id: 18 },
    { id: 19 },
    { id: 20 },
    { id: 21 },
    { id: 29 },
    { id: 210 },
    { id: 211 },
    { id: 212 },
    { id: 213 },
    { id: 214 },
    { id: 215 },
    { id: 216 },
    { id: 217 },
    { id: 218 },
    { id: 219 },
    { id: 220 },
    { id: 221 },
    { id: 310 },
    { id: 311 },
    { id: 312 },
    { id: 313 },
    { id: 314 },
    { id: 315 },
    { id: 316 },
    { id: 317 },
    { id: 318 },
    { id: 319 },
    { id: 330 },
    { id: 331 },
];

export const data2 = [
    { id: 1 },
    { id: 2 },
    { id: 3 },
    { id: 4 },
    { id: 5 },
    { id: 6 },
    { id: 7 },
    { id: 8 },
];

export const browseByCategoriesData = [
    {
        id: 1,
        slug: "All Genres",
        icon: "/icons/browse-by-category-icons/tick.svg",
    },
    {
        id: 2,
        slug: "Art",
        icon: "/icons/browse-by-category-icons/head-brain.svg",
    },
    {
        id: 3,
        slug: "Education",
        icon: "/icons/browse-by-category-icons/edu.svg",
    },
    {
        id: 4,
        slug: "Comedy",
        icon: "/icons/browse-by-category-icons/mask.svg",
    },
    {
        id: 5,
        slug: "Kids & Family",
        icon: "/icons/browse-by-category-icons/horse.svg",
    },
    {
        id: 6,
        slug: "TV & Film",
        icon: "/icons/browse-by-category-icons/tv.svg",
    },
    {
        id: 7,
        slug: "Religion",
        icon: "/icons/browse-by-category-icons/church.svg",
    },
    {
        id: 8,
        slug: "Technology",
        icon: "/icons/browse-by-category-icons/atom.svg",
    },
];

export const popularCategoriesData = [
    {
        id: 1,
        slug: "All Genres",
        icon: "/images/popular-cat/1.png",
    },
    {
        id: 2,
        slug: "Art",
        icon: "/images/popular-cat/2.png",
    },
    {
        id: 3,
        slug: "Education",
        icon: "/images/popular-cat/3.png",
    },
    {
        id: 4,
        slug: "Comedy",
        icon: "/images/popular-cat/4.png",
    },
    {
        id: 5,
        slug: "Kids & Family",
        icon: "/images/popular-cat/1.png",
    },
    {
        id: 6,
        slug: "TV & Film",
        icon: "/images/popular-cat/2.png",
    },
    {
        id: 7,
        slug: "Religion",
        icon: "/images/popular-cat/3.png",
    },
    {
        id: 8,
        slug: "Technology",
        icon: "/images/popular-cat/4.png",
    },
    {
        id: 9,
        slug: "All Genres",
        icon: "/images/popular-cat/1.png",
    },
    {
        id: 12,
        slug: "Art",
        icon: "/images/popular-cat/2.png",
    },
    {
        id: 13,
        slug: "Education",
        icon: "/images/popular-cat/3.png",
    },
    {
        id: 14,
        slug: "Comedy",
        icon: "/images/popular-cat/4.png",
    },
    {
        id: 15,
        slug: "Kids & Family",
        icon: "/images/popular-cat/1.png",
    },
    {
        id: 16,
        slug: "TV & Film",
        icon: "/images/popular-cat/2.png",
    },
    {
        id: 17,
        slug: "Religion",
        icon: "/images/popular-cat/3.png",
    },
    {
        id: 18,
        slug: "Technology",
        icon: "/images/popular-cat/4.png",
    },
    {
        id: 21,
        slug: "All Genres",
        icon: "/images/popular-cat/1.png",
    },
    {
        id: 22,
        slug: "Art",
        icon: "/images/popular-cat/2.png",
    },
    {
        id: 23,
        slug: "Education",
        icon: "/images/popular-cat/3.png",
    },
    {
        id: 24,
        slug: "Comedy",
        icon: "/images/popular-cat/4.png",
    },
];

export const cardRectBigData = [
    {
        id: 1,
        slug: "All Genres",
        icon: "/images/card-big/1.svg",
    },
    {
        id: 2,
        slug: "Art",
        icon: "/images/card-big/1.svg",
    },
    {
        id: 3,
        slug: "Education",
        icon: "/images/card-big/1.svg",
    },
    {
        id: 4,
        slug: "Comedy",
        icon: "/images/card-big/1.svg",
    },
    {
        id: 5,
        slug: "Kids & Family",
        icon: "/images/card-big/1.svg",
    },
    {
        id: 6,
        slug: "TV & Film",
        icon: "/images/card-big/1.svg",
    },
    {
        id: 7,
        slug: "Religion",
        icon: "/images/card-big/1.svg",
    },
    {
        id: 8,
        slug: "Technology",
        icon: "/images/card-big/1.svg",
    },
];

export const cardRectSmallData = [
    {
        id: 1,
        slug: "All Genres",
        icon: "/images/card-small/1.svg",
    },
    {
        id: 2,
        slug: "Art",
        icon: "/images/card-small/2.svg",
    },
    {
        id: 3,
        slug: "Education",
        icon: "/images/card-small/1.svg",
    },
    {
        id: 4,
        slug: "Comedy",
        icon: "/images/card-small/1.svg",
    },
    {
        id: 5,
        slug: "Kids & Family",
        icon: "/images/card-small/2.svg",
    },
    {
        id: 6,
        slug: "TV & Film",
        icon: "/images/card-small/1.svg",
    },
    {
        id: 7,
        slug: "Religion",
        icon: "/images/card-small/2.svg",
    },
    {
        id: 8,
        slug: "Technology",
        icon: "/images/card-small/1.svg",
    },
];

export const unwindData = [
    {
        id: 1,
        slug: "All Genres",
        icon: "/images/unwind/1.svg",
    },
    {
        id: 2,
        slug: "Art",
        icon: "/images/unwind/1.svg",
    },
    {
        id: 3,
        slug: "Education",
        icon: "/images/unwind/1.svg",
    },
    {
        id: 4,
        slug: "Comedy",
        icon: "/images/unwind/1.svg",
    },
    {
        id: 5,
        slug: "Kids & Family",
        icon: "/images/unwind/1.svg",
    },
    {
        id: 6,
        slug: "TV & Film",
        icon: "/images/unwind/1.svg",
    },
    {
        id: 7,
        slug: "Religion",
        icon: "/images/unwind/1.svg",
    },
    {
        id: 8,
        slug: "Technology",
        icon: "/images/unwind/1.svg",
    },
];

export const historyhitData = [
    {
        id: 1,
        slug: "All Genres",
        icon: "/images/history-hit/1.svg",
    },
    {
        id: 2,
        slug: "Art",
        icon: "/images/history-hit/1.svg",
    },
    {
        id: 3,
        slug: "Education",
        icon: "/images/history-hit/1.svg",
    },
    {
        id: 4,
        slug: "Comedy",
        icon: "/images/history-hit/1.svg",
    },
    {
        id: 5,
        slug: "Kids & Family",
        icon: "/images/history-hit/1.svg",
    },
    {
        id: 6,
        slug: "TV & Film",
        icon: "/images/history-hit/1.svg",
    },
    {
        id: 7,
        slug: "Religion",
        icon: "/images/history-hit/1.svg",
    },
    {
        id: 8,
        slug: "Technology",
        icon: "/images/history-hit/1.svg",
    },
];

export const componentOrder = [
    {
        platform: "web",
        sectionTab: "podcast",
        active: "yes",
        created: "27/11/2023",
        createdBy: "Woodenclouds",
        valid: "this document's valid duration",
        section: [
            {
                position: "1",
                designName: "PromotedSlideSection",
                categoryToShow: "P",
                isCollection: "yes",
                apiUrl: "collection/items",
                item: {
                    enabled: "yes",
                    heading: [],
                    subHeading: [],
                    contentType: "P",
                    id: "6420041ad57b4dadda75573d",
                    method: "GET",
                },
            },
            {
                position: "2",
                designName: "FeaturedPodcast",
                categoryToShow: "P",
                isCollection: "yes",
                apiUrl: "collection/items",
                item: {
                    enabled: "yes",
                    heading: [],
                    subHeading: [],
                    contentType: "P",
                    id: "6420041ad57b4dadda75573d",
                    method: "GET",
                },
            },
            {
                position: "2",
                designName: "PodLemonChannel",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "3",
                designName: "PromotedSection",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "4",
                designName: "TrendingPodcast",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "5",
                designName: "EpisodeSection",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "6",
                designName: "GeneralListing",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "7",
                designName: "SuggestedPodcasts",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "8",
                designName: "SponsoredSection",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "9",
                designName: "HistoryHit",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "10",
                designName: "UnwindWithCalm",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "11",
                designName: "CardRectangleSmall",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "12",
                designName: "CardRectangleBig",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "13",
                designName: "PopularCategories",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
            {
                position: "14",
                designName: "BrowseByCategories",
                heading: "Music section",
                subHeading: "The most popular Music",
                isCollection: "no",
                apiUrl: {
                    url: "music/trending/podcast",
                    limit: "5",
                    page: "1",
                    method: "GET",
                },
            },
        ],
    },
];
