export const resolutions = {
    spotlight: 520,
    featured: 300,
    podlemonChannel: 250,
    promoted: 250,
    trendingPodcasts: 200,
    episode: 100,
    generalListing: 250,
    suggestedPodcast: 200,
    sponsored: 200,
    historyHit: 250,
    unwindwithCalm: 250,
    cardRectangleSmall: {
        w: 250,
        h: 130,
    },
    cardRectangleBig: {
        w: 340,
        h: 217,
    },
    popularCategories: 75,
};
