export const getDominantColor = async (imageSrc, ratio) => {
    return new Promise((resolve, reject) => {
        const image = new Image();
        image.crossOrigin = "Anonymous";
        image.onload = () => {
            const canvas = document.createElement("canvas");
            let height = (canvas.height = image.naturalHeight);
            let width = (canvas.width = image.naturalWidth);
            const context = canvas.getContext("2d");
            context.drawImage(image, 0, 0);
            let data, length;
            let i = -4,
                count = 0;
            try {
                data = context.getImageData(0, 0, width, height);
                length = data.data.length;
            } catch (err) {
                console.error(err);
                reject(err);
            }
            let R = 0,
                G = 0,
                B = 0;
            while ((i += ratio * 4) < length) {
                ++count;
                R += data.data[i];
                G += data.data[i + 1];
                B += data.data[i + 2];
            }
            R = ~~(R / count);
            G = ~~(G / count);
            B = ~~(B / count);
            resolve({ R, G, B });
        };
        image.src = imageSrc;
    });
};

export const shortDate = (dateString) => {
    const date = new Date(dateString);
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    // Pad single digit day and month with leading zero if needed
    const formattedDay = String(day).padStart(2, "0");
    const formattedMonth = String(month).padStart(2, "0");

    return `${formattedMonth}/${formattedDay}/${year}`;
};

export const removeTags = (htmlContent) => {
    if (htmlContent && typeof htmlContent === "string") {
        return htmlContent
            .replace(/<[^>]*>/g, "")
            .replace(/<\/?p>|<br\s*\/?>/g, "");
    } else {
        return ""; // or any other default value you prefer
    }
};

export const longDate = (dateString) => {
    const date = new Date(dateString);
    const options = { month: "short", day: "numeric", year: "numeric" };
    return date.toLocaleDateString("en-US", options);
};

export const shorttDateAndMonth = (dateString) => {
    const date = new Date(dateString);
    const months = [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
    ];
    const day = date.getDate();
    const month = months[date.getMonth()];
    return `${day} ${month}`;
};

export const colorToHex = (color) => {
    const [r, g, b] = color;
    return `#${r.toString(16).padStart(2, "0")}${g
        .toString(16)
        .padStart(2, "0")}${b.toString(16).padStart(2, "0")}`;
};

export const optimizeImage = (id, originalUrl, resolution) => {
    const url = `http://covers.netsip.co.uk/imgConvert.php/${id}/${resolution}x${resolution}?url=${originalUrl}`;
    return url;
};
