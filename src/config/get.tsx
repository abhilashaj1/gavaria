export default async function get(path: string, data: any) {
    try {
        const response = await fetch(`${path}`, {
            cache: "force-cache",
            headers: {
                "Content-Type": "application/json",
            },
        });
        if (!response.ok) {
            throw new Error(`HTTP error! Status: ${response.status}`);
        }

        return response.json();
    } catch (error) {
        console.error("An error occurred:", error);
        throw error;
    }
}
