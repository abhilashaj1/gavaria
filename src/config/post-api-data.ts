import configVariables from "@/variables/configVariables";

export default async function postApiData(path: string, bodyData: any = null) {
    let accessToken =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdF9uYW1lIjoiQWRtaW4iLCJsYXN0X25hbWUiOiJVc2VyIiwiZW1haWwiOiJhZG1pbkB5b3BtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIn0sImlhdCI6MTY5MDIwMDI2OCwiZXhwIjoxNzIxNzU3ODY4fQ.vPiZifNO34I0YQMOcvBi-A3hw6jHDJ3B8cuEPgUpvCM";
    const baseUrl = configVariables.baseUrl;

    try {
        let headersData: HeadersInit = new Headers();
        if (accessToken) {
            headersData.append("Authorization", `Bearer ${accessToken}`);
        }

        let options: RequestInit = {
            method: "POST",
            headers: headersData,
        };

        if (bodyData) {
            const formData = new FormData();

            Object.keys(bodyData).forEach((key) => {
                if (Array.isArray(bodyData[key])) {
                    bodyData[key].forEach((data: any) => {
                        formData.append(`${key}`, data);
                    });
                } else {
                    formData.append(key, bodyData[key]);
                }
            });

            options.body = formData;
        }

        const response = await fetch(`${baseUrl}${path}`, options);

        console.log("responseData", response);

        const responseData = await response.json();

        if (!response.ok) {
            throw new Error(responseData.message || "Failed to fetch");
        }

        return responseData;
    } catch (error: any) {
        console.error("Error in postApiData:", error);
        return { error: error.message || "An unknown error occurred" };
    }
}
