import configVariables from "@/variables/configVariables";

export default async function fetchApiData(path: string, data?: any) {
    let accessToken =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdF9uYW1lIjoiQWRtaW4iLCJsYXN0X25hbWUiOiJVc2VyIiwiZW1haWwiOiJhZG1pbkB5b3BtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIn0sImlhdCI6MTY5MDIwMDI2OCwiZXhwIjoxNzIxNzU3ODY4fQ.vPiZifNO34I0YQMOcvBi-A3hw6jHDJ3B8cuEPgUpvCM";
    const baseUrl = configVariables.baseUrl;
    try {
        let headersData = {};
        if (accessToken) {
            headersData = {
                "Content-Type": "application/json",
                Authorization: `Bearer ` + accessToken,
            };
        }
        const response = await fetch(`${baseUrl}${path}`, {
            cache: "force-cache",
            headers: headersData,
        });

        const responseData = await response.json(); // Attempt to parse JSON regardless of response status

        return responseData;
    } catch (error) {
        return error;
    }
}
