const server = "demo";
// const server = "live";

const localBaseUrl = "";
const demoBaseUrl = "http://apidev.gavaria.com/api/";

const liveBaseUrl = "";

const configVariables = {
    baseUrl:
        server === "demo"
            ? demoBaseUrl
            : server === "live"
            ? liveBaseUrl
            : localBaseUrl,
};

export default configVariables;
