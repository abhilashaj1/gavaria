import SpotlightHeader from "@/components/general/SpotlightHeader";
import get from "@/config/get";
import dynamic from "next/dynamic";
import React from "react";
import { RadioSigleSection } from "./_components/RadioSigleSection";

async function page() {
    const sections = await get(
        "https://woodenclouds.in/demo/gavaria/web/radio",
        {}
    );
    console.log(sections, "sections");

    return (
        <div>
            <SpotlightHeader />
            <RadioSigleSection sections={sections} />
        </div>
    );
}

export default page;
