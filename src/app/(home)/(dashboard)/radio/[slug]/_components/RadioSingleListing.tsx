import React, { useRef, useCallback } from "react";
import Image from "next/image";
import { useInfiniteQuery } from "@tanstack/react-query"; // Importing useInfiniteQuery from react-query
import { optimizeImage } from "@/lib/functions";
import { resolutions } from "@/lib/constants";
import fetchApiData from "@/config/fetch-api-data";
import Followbutton from "@/components/general/buttons/Followbutton";
import PlayButton from "@/components/general/buttons/PlayButton";
import Menubutton from "@/components/general/buttons/Menubutton";
import SectionHeading from "./SectionHeading";

const fetchData = async ({ url, pageParam = 1, limit }: any) => {
    const data = await fetchApiData(`${url}?page=${pageParam}`);
    return data;
};

const RadioSingleListing = ({ sectionsDetails }: any) => {
    const limit = sectionsDetails?.apiUrl?.limit || 20;
    const url = sectionsDetails?.apiUrl.url;

    const { data, isLoading, isError, fetchNextPage, hasNextPage } =
        useInfiniteQuery({
            queryKey: ["radioData", sectionsDetails?.apiUrl.url, limit],
            queryFn: ({ pageParam }) => fetchData({ url, pageParam, limit }),
            getNextPageParam: (lastPage) => lastPage.nextPage ?? false,
            initialPageParam: 1, // Add this line to specify the initial page parameter
        });

    // Explicitly set the type of observerRef
    const observerRef = useRef<IntersectionObserver | null>(null);

    const lastElementRef = useCallback(
        (node: any) => {
            if (isLoading) return;
            if (observerRef.current) observerRef.current.disconnect();
            observerRef.current = new IntersectionObserver((entries) => {
                if (entries[0].isIntersecting && hasNextPage) {
                    fetchNextPage();
                }
            });
            if (node) observerRef.current.observe(node);
        },
        [isLoading, fetchNextPage, hasNextPage]
    );

    if (isLoading) {
        return (
            <div
                style={{
                    display: "flex",
                    justifyContent: "center",
                    alignItems: "center",
                    minHeight: "70dvh",
                }}
            >
                <div className="text-center text-[16px] flex justify-center items-center min-h-[300px]">
                    <div className="table-loader"></div>
                </div>
            </div>
        );
    }

    if (isError) {
        return (
            <div className="flex justify-center items-center min-h-[80dvh] primary-color text-5xl">
                Failed to load data.
            </div>
        );
    }

    return (
        <section className="relative mt-[-15px] mx-auto w-full pt-1">
            <>
                <SectionHeading title={sectionsDetails.heading} />
                <div className="flex flex-wrap mx-[45px] my-0 pt-[3px]">
                    {data &&
                        data.pages.map((page: any, pageIndex: any) => (
                            <React.Fragment key={pageIndex}>
                                {page?.docs?.map(
                                    (item: any, itemIndex: any) => {
                                        const isLastElement =
                                            pageIndex ===
                                                data.pages.length - 1 &&
                                            itemIndex ===
                                                page?.docs?.length - 1;
                                        return (
                                            <div
                                                className={`relative w-[calc(14%--19px)] md:w-[calc(14%--19px)] sm:w-[calc(29.33%-11px)] xs:w-[calc(50%-11px)] ${
                                                    (itemIndex + 1) % 6 === 0
                                                        ? "mr-0"
                                                        : "mr-[20px]"
                                                }`}
                                                key={item.id}
                                                ref={
                                                    isLastElement
                                                        ? lastElementRef
                                                        : null
                                                }
                                            >
                                                <FeaturedCard cardData={item} />
                                            </div>
                                        );
                                    }
                                )}
                            </React.Fragment>
                        ))}
                </div>
            </>
        </section>
    );
};

const FeaturedCard = ({ cardData }: any) => {
    return (
        <div className="h-[85%]">
            <div className="absolute top-[2%] right-[3%] cursor-pointer z-10">
                <Menubutton width={30} height={30} />
            </div>
            <div>
                <div className="absolute bottom-[33%] right-[3%] cursor-pointer z-10">
                    <Followbutton width={30} height={30} />
                </div>
                <div className="absolute bottom-[33%] left-[3%] cursor-pointer z-10">
                    <PlayButton width={30} height={30} />
                </div>
            </div>
            <div className="relative h-[80%] overflow-hidden cursor-pointer transition-all duration-300 ease-in-out outline outline-2 outline-transparent hover:outline-white rounded-lg ">
                <Image
                    unoptimized={true}
                    // src={optimizeImage(
                    //     cardData?.id,
                    //     cardData?.favicon ||
                    //         "http://covers.netsip.co.uk/imgConvert.php/61eb3ef3-0787-4685-9345-a28a146ab391/200x200?url=https://anthemz.nz/wp-content/uploads/2020/09/logo-sm.png&resourceType=radio",
                    //     resolutions.trendingPodcasts
                    // )}
                    src={
                        cardData?.favicon
                            ? cardData?.favicon
                            : "http://covers.netsip.co.uk/imgConvert.php/61eb3ef3-0787-4685-9345-a28a146ab391/200x200?url=https://anthemz.nz/wp-content/uploads/2020/09/logo-sm.png&resourceType=radio"
                    }
                    width={1000}
                    height={1000}
                    alt="Image"
                    className="rounded-lg w-[100%] h-[100%] transform transition-transform duration-300 ease-in-out hover:scale-105"
                />
            </div>
            <div className="mt-1.5">
                <h1 className="primary-color mt-[2px] overflow-hidden whitespace-nowrap overflow-ellipsis text-[18px] font-medium">
                    {cardData?.name}
                </h1>
                <div
                    style={{
                        display: "flex",
                        alignItems: "center",
                    }}
                >
                    <Image
                        src={cardData?.flag}
                        width={25}
                        height={25}
                        alt="Image"
                        loading="lazy"
                        style={{
                            marginRight: "10px",
                        }}
                    />
                    <h2 className="secondry-color overflow-hidden whitespace-nowrap overflow-ellipsis text-[14px] font-normal">
                        {cardData?.country}
                    </h2>
                </div>
            </div>
        </div>
    );
};

export default RadioSingleListing;
