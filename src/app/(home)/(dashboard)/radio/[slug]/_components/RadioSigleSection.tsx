"use client";
import React from "react";
import { useSearchParams } from "next/navigation";
import RadioSingleListing from "./RadioSingleListing";
export const RadioSigleSection = ({ sections }: any) => {
    const searchParams = useSearchParams();
    const search = searchParams.get("id");

    const renderSections = () => {
        if (!sections) {
            return (
                <div
                    style={{
                        display: "flex",
                        justifyContent: "center",
                        alignItems: "center",
                        minHeight: "100dvh",
                    }}
                >
                    <div className="text-center text-[16px] flex justify-center items-center min-h-[300px]">
                        <div className="table-loader"></div>
                    </div>
                </div>
            );
        }

        const FeaturedPodcast = search;
        return sections?.section?.map((section: any, index: any) => {
            console.log(section, "section");

            switch (section.position) {
                case FeaturedPodcast:
                    return (
                        <RadioSingleListing
                            key={index}
                            sectionsDetails={section}
                        />
                    );

                default:
                    return null;
            }
        });
    };

    return (
        <div>
            <section className="wrapper">{renderSections()}</section>
        </div>
    );
};
