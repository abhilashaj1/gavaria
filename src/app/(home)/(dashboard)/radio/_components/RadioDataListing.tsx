import fetchApiData from "@/config/fetch-api-data";
import React from "react";
import { DoubleRowSection } from "./DoubleRowSection";
import { SingleRowSection } from "./SingleRowSection";

async function RadioDataListing({ sectionsDetails, isdouble }: any) {
    const data = await fetchApiData(
        `${sectionsDetails?.apiUrl.url}/?limit=${sectionsDetails?.apiUrl?.limit}`
    );

    if (data) {
        console.log("Data fetched successfully:", data);
    } else {
        console.error("Failed to fetch data.");
    }
    return isdouble ? (
        <DoubleRowSection data={data.docs} sectionsDetails={sectionsDetails} />
    ) : (
        <SingleRowSection data={data.docs} sectionsDetails={sectionsDetails} />
    );
}

export default RadioDataListing;
