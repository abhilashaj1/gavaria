import get from "@/config/get";
import RadioDataListing from "./RadioDataListing";

export default async function RadioSection() {
    const sections = await get(
        "https://woodenclouds.in/demo/gavaria/web/radio",
        {}
    );

    const renderSections = () =>
        sections?.section?.map((section: any, index: any) => {
            switch (section?.designName) {
                case "RadioByRandom":
                    return (
                        <RadioDataListing
                            key={index}
                            sectionsDetails={section}
                            isdouble
                        />
                    );
                case "RadioByLikes":
                    return (
                        <RadioDataListing
                            key={index}
                            sectionsDetails={section}
                        />
                    );
                case "RadioByTrending":
                    return (
                        <RadioDataListing
                            key={index}
                            sectionsDetails={section}
                            isdouble
                        />
                    );
                case "RadioByRecentlyChanged":
                    return (
                        <RadioDataListing
                            key={index}
                            sectionsDetails={section}
                            isdouble
                        />
                    );
                default:
                    return null;
            }
        });

    return (
        <div>
            <section className="wrapper">
                {sections?.section > 0 && (
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            minHeight: "100dvh",
                        }}
                    >
                        <div className="text-center text-[16px] flex justify-center items-center min-h-[300px]">
                            <div className="table-loader"></div>
                        </div>
                    </div>
                )}
                {sections && renderSections()}
            </section>
        </div>
    );
}
