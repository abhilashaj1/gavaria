"use client";
import React, { useRef, useState, useEffect } from "react";
import Link from "next/link";

export const SectionHeader = ({
    title,
    description,
    isSeeAll = false,
    isSliderIcons = true,
    handleScrollLeft,
    handleScrollRight,
    designName,
    requrid,
    position,
}: any) => {
    const containerRef = useRef(null);
    const [isLargeScreen, setIsLargeScreen] = useState(false);

    useEffect(() => {
        const checkScreenSize = () => {
            setIsLargeScreen(window.innerWidth > 1024); // Adjust the breakpoint as needed
        };

        checkScreenSize();

        window.addEventListener("resize", checkScreenSize);
        return () => window.removeEventListener("resize", checkScreenSize);
    }, []);

    return (
        <section className="py-4 px-6 flex items-center justify-between">
            <div className="flex-1">
                <h1 className="primary-color text-[24px] font-medium overflow-hidden whitespace-nowrap overflow-ellipsis">
                    {title}
                </h1>
                <p className="secondry-color text-[13px]  overflow-hidden whitespace-nowrap overflow-ellipsis">
                    {description}
                </p>
            </div>
            <div className="flex items-center">
                {isSeeAll && isLargeScreen && !requrid && (
                    <Link
                        href={`/radio/${designName}/?id=${position}`}
                        className="primary-color text-sm mr-4"
                    >
                        See All
                    </Link>
                )}
                {isSeeAll && isLargeScreen && (
                    <div className="flex items-center">
                        <div
                            className="mr-4 cursor-pointer"
                            onClick={handleScrollLeft}
                        >
                            <img
                                src="/icons/left-arrow.svg"
                                alt="Left Arrow"
                                className="w-5 h-5"
                            />
                        </div>
                        <div
                            className="cursor-pointer"
                            onClick={handleScrollRight}
                        >
                            <img
                                src="/icons/right-arrow.svg"
                                alt="Right Arrow"
                                className="w-5 h-5"
                            />
                        </div>
                    </div>
                )}
            </div>
        </section>
    );
};
