import React from "react";
import AudioBookSection from "./_components/AudioBookSection";

function page() {
    return <AudioBookSection />;
}

export default page;
