import Image from "next/image";
import Link from "next/link";

function AudioSectionHeading({ title }: any) {
    return (
        <div className="px-4 py-5 md:px-8 md:py-6 flex items-center justify-between">
            <Link href="/audiobooks" className="flex items-center space-x-2">
                <Image
                    src={"/icons/podcast_listing.svg"}
                    width={10}
                    height={19}
                    alt="Image"
                    loading="lazy"
                />
                <h1 className="text-[24px] font-medium primary-color">
                    {title}
                </h1>
            </Link>
            <div>
                <Image
                    src={"/icons/music-Menu.svg"}
                    width={31}
                    height={31}
                    alt="Image"
                    loading="lazy"
                />
            </div>
        </div>
    );
}

export default AudioSectionHeading;
