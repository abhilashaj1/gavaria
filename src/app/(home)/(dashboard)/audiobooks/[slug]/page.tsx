import SpotlightHeader from "@/components/general/SpotlightHeader";
import get from "@/config/get";
import dynamic from "next/dynamic";
import React from "react";
import { AudioSingleSection } from "./_components/AudioSingleSection";

async function page() {
    const sections = await get(
        "http://woodenclouds.in/demo/gavaria/web/audiobook",
        {}
    );

    return (
        <div>
            <SpotlightHeader />
            <AudioSingleSection sections={sections} />
        </div>
    );
}

export default page;
