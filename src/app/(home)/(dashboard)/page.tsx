// "use client";
// import React, { useEffect, useState } from "react";
// import { FeaturedPodcast } from "./FeaturedPodcast.jsx/FeaturedPodcast";
// import { PodlemonChannel } from "./PodlemonChannel/PodlemonChannel";
// import { Promoted } from "./Promoted/Promoted";
// import { TrendingPodcasts } from "./TrendingPodcasts/TrendingPodcasts";
// import { Episode } from "./Episode/Episode";
// import { GeneralListing } from "./GeneralListing/GeneralListing";
// import { SuggestedPodcast } from "./SuggestedPodcast/SuggestedPodcast";
// import { Sponsored } from "./Sponsored/Sponsored";
// import { HistoryHit } from "./HistoryHit/HistoryHit";
// import { UnwindwithCalm } from "./UnwindwithCalm/UnwindwithCalm";
// import { CardRectangleSmall } from "./CardRectangleSmall/CardRectangleSmall";
// import { CardRectangleBig } from "./CardRectangleBig/CardRectangleBig";
// import { PopularCategories } from "./PopularCategories/PopularCategories";
// import { BrowsebyCategories } from "./BrowsebyCategories/BrowsebyCategories";
// import useSWR from "swr";

// const fetcher = async (url, accessToken) => {
//     const response = await fetch(url, {
//         cache: "force-cache",
//     });
//     const data = await response.json();
//     return data;
// };

// export const PodcastSections = () => {
//     const baseUrl = "http://woodenclouds.in/demo/gavaria/web/podcast";
//     const {
//         data: sections,
//         isError,
//         isLoading,
//     } = useSWR(baseUrl, (url) => fetcher(url), {
//         dedupingInterval: 3600000,
//         refreshInterval: 3600000,
//         revalidateOnFocus: false,
//         revalidateOnReconnect: false,
//     });

//     const renderSections = () => {
//         return sections?.section?.map((section, index) => {
//             switch (section.designName) {
//                 case "FeaturedPodcast":
//                     return (
//                         <FeaturedPodcast
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "PodLemonChannel":
//                     return (
//                         <PodlemonChannel
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={true}
//                         />
//                     );
//                 case "PromotedSection":
//                     return (
//                         <Promoted
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "TrendingPodcast":
//                     return (
//                         <TrendingPodcasts
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "EpisodeSection":
//                     return (
//                         <Episode
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={true}
//                         />
//                     );
//                 case "GeneralListing":
//                     return (
//                         <GeneralListing
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "SuggestedPodcasts":
//                     return (
//                         <SuggestedPodcast
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "SponsoredSection":
//                     return (
//                         <Sponsored
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "HistoryHit":
//                     return (
//                         <HistoryHit
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "UnwindWithCalm":
//                     return (
//                         <UnwindwithCalm
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "CardRectangleSmall":
//                     return (
//                         <CardRectangleSmall
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "CardRectangleBig":
//                     return (
//                         <CardRectangleBig
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "PopularCategories":
//                     return (
//                         <PopularCategories
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 case "BrowseByCategories":
//                     return (
//                         <BrowsebyCategories
//                             key={index}
//                             sectionsDetails={section}
//                             isMeta={false}
//                         />
//                     );
//                 default:
//                     return null;
//             }
//         });
//     };
//     return (
//         <div>
//             <section className="wrapper">
//                 {isLoading ? (
//                     <div
//                         style={{
//                             display: "flex",
//                             flexDirection: "column",
//                             alignItems: "center",
//                             justifyContent: "center",
//                             height: "80dvh",
//                         }}
//                     >
//                         <h1
//                             style={{
//                                 color: "#fff",
//                                 fontSize: "30px",
//                                 textAlign: "center",
//                             }}
//                         >
//                             Loading
//                         </h1>
//                     </div>
//                 ) : (
//                     renderSections()
//                 )}
//             </section>
//         </div>
//     );
// };
import get from "@/config/get";
import PodcastListing from "./_components/PodcastListing";
import { BrowsebyCategories } from "./_components/BrowsebyCategories";

export default async function Home() {
    const sections = await get(
        "http://woodenclouds.in/demo/gavaria/web/podcast",
        {}
    );

    const renderSections = () =>
        sections?.section?.map((section: any, index: any) => {
            switch (section?.designName) {
                case "SuggestedPodcasts":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={""}
                        />
                    );
                case "GeneralListing":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={"issingle"}
                        />
                    );
                case "TrendingPodcast":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={"isdouble"}
                        />
                    );
                case "CardRectangleSmall":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={"issmall"}
                        />
                    );
                case "CardRectangleBig":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={"isbig"}
                        />
                    );
                case "FeaturedPodcast":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={"featured"}
                        />
                    );
                case "SponsoredSection":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={"promoted"}
                        />
                    );
                case "HistoryHit":
                    return (
                        <PodcastListing
                            key={index}
                            sectionsDetails={section}
                            type={"hits"}
                        />
                    );
                case "BrowseByCategories":
                    return (
                        <BrowsebyCategories
                            sectionsDetails={section}
                            key={index}
                        />
                    );

                default:
                    return null;
            }
        });

    return (
        <div>
            <section className="wrapper">
                {sections?.section > 0 && (
                    <div
                        style={{
                            display: "flex",
                            justifyContent: "center",
                            alignItems: "center",
                            minHeight: "100dvh",
                        }}
                    >
                        <div className="text-center text-[16px] flex justify-center items-center min-h-[300px]">
                            <div className="table-loader"></div>
                        </div>
                    </div>
                )}
                {sections && renderSections()}
            </section>
        </div>
    );
}
