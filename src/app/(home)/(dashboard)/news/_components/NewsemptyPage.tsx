import Image from "next/image";
import React from "react";

function NewsemptyPage() {
    return (
        <div
            style={{
                width: "100%",
                height: "100vh",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
            }}
        >
            <Image
                src={"/images/com.png"}
                width={700}
                height={500}
                objectFit="cover"
                alt="Image"
                priority
            />
        </div>
    );
}

export default NewsemptyPage;
