import dynamic from "next/dynamic";
import React from "react";

const NewsemptyPage = dynamic(() => import("./_components/NewsemptyPage"), {
    ssr: true,
});

function page() {
    return <NewsemptyPage />;
}

export default page;
