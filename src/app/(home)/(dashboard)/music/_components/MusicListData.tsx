"use client";
import React, { useEffect, useState } from "react";
import Image from "next/image";
import { removeTags } from "@/lib/functions";
import { resolutions } from "@/lib/constants";
import preImage from "../../../../../../public/images/pre_image.png";
import PlayButton from "@/components/general/buttons/PlayButton";
import Menubutton from "@/components/general/buttons/Menubutton";
import SkeletonLoader from "@/components/general/SkeletonLoader";

export const MusicListData = ({ listData }: any) => {
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (listData?.length > 0) {
            setIsLoading(false);
        }
    }, [listData]);
    return listData && listData?.length > 0 ? (
        <section className="pt-[4rem]  px-[0] pb-[0] relative -mt-[15px] w-full mx-[auto] my-[0] max-w-[75rem] contents">
            <div className="flex flex-wrap ml-[2rem] my-[0] pt-[4rem] ">
                {listData
                    ? listData.map((item: any, index: any) => (
                          <FeaturedCard
                              key={index}
                              cardData={item}
                              index={index}
                              isLoading={isLoading}
                          />
                      ))
                    : // Render skeleton loaders while data is loading
                      Array.from({ length: 10 }).map((_, index) => (
                          <SkeletonLoader key={index} />
                      ))}
            </div>
        </section>
    ) : (
        <div
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                minHeight: "100dvh",
            }}
        >
            <div className="text-center text-[16px] flex justify-center items-center min-h-[300px] ">
                <div className="table-loader"></div>
            </div>
        </div>
    );
};

const FeaturedCard = ({ cardData, index, isLoading }: any) => {
    const [imageLoadError, setImageLoadError] = useState(false);
    const [imageLoading, setImageLoading] = useState(true);

    const handleImageLoad = () => {
        setImageLoading(false);
    };

    const handleImageError = () => {
        setImageLoadError(true);
    };

    const marginRightClass = index % 5 === 4 ? "" : "mr-[20px]";

    return (
        <div
            className={`relative mb-5 w-[calc(18.4%-0px)] md:w-[calc(18.4%-0px)] sm:w-[calc(33.33%-15px)] xs:w-[calc(50%-15px)] ${marginRightClass}`}
        >
            <div className="relative cursor-pointer overflow-hidden rounded-md transition duration-300 ease-in-out pod_image h-[80%]">
                {!imageLoadError && (
                    <>
                        <div className="absolute right-[3%] top-[2%] z-10 cursor-pointer">
                            <Menubutton width={35} height={35} />
                        </div>
                        <div className="absolute right-[3%] bottom-[2%] z-10 cursor-pointer">
                            <PlayButton width={35} height={35} />
                        </div>
                    </>
                )}
                <Image
                    unoptimized={true}
                    src={
                        !isLoading ? cardData?.feedImage : preImage
                        // ? cardData?.image
                        // : `http://covers.netsip.co.uk/imgConvert.php/${cardData?.id}/${resolutions.featured}x${resolutions.featured}?url=${cardData?.image}`
                    }
                    width={10}
                    height={10}
                    alt="Podcast Image"
                    className="[transition:0.3s_ease] h-full w-full min-h-[80%] overflow-hidden hover:scale-105 hover:[transition:0.3s_ease] "
                    style={{
                        display: imageLoading ? "none" : "block",
                    }}
                    onError={handleImageError}
                    onLoad={handleImageLoad}
                    priority
                />

                {(imageLoadError || !cardData?.image) && (
                    <Image
                        unoptimized={true}
                        src={preImage}
                        width={10}
                        height={10}
                        alt="Podcast Image"
                        className="w-full h-full min-h-[80%] rounded-md"
                        style={{
                            display: imageLoading ? "block" : "none",
                        }}
                    />
                )}
            </div>
            <div className="mt-1.5 md:mt-1">
                <h1 className="font-medium primary-color overflow-hidden text-ellipsis whitespace-nowrap ">
                    {cardData?.title}
                </h1>
                <h2 className="text-[14px] font-normal secondry-color overflow-hidden text-ellipsis whitespace-nowrap ">
                    {removeTags(cardData?.author)}
                </h2>
            </div>
        </div>
    );
};

export default MusicListData;
