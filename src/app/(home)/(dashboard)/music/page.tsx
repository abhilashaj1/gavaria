import fetchApiData from "@/config/fetch-api-data";
import dynamic from "next/dynamic";
import NodeCache from "node-cache";

const musicCache = new NodeCache({ stdTTL: 3600 });
const MUSIC_DATA_CACHE_KEY = "music-trending-data";
const MusicListData = dynamic(() => import("./_components/MusicListData"), {
    ssr: false,
});

async function getMusicData() {
    if (musicCache.has(MUSIC_DATA_CACHE_KEY)) {
        const cachedData = musicCache.get(MUSIC_DATA_CACHE_KEY);
        console.log("Music data retrieved from cache");
        return cachedData;
    }

    // Fetch data from API if not cached
    const path = "/music/trending?limit=8&page=1";
    const data = await fetchApiData(path);
    if (data instanceof Error) {
        throw data;
    }

    musicCache.set(MUSIC_DATA_CACHE_KEY, data);
    console.log("Music data stored in cache");

    return data;
}

async function page() {
    try {
        const data = await getMusicData();
        return <MusicListData listData={data?.docs} />;
    } catch (error: any) {
        return <div>Error fetching music data: {error.message}</div>;
    }
}

export default page;
