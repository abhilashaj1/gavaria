"use client";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import { fetchData } from "../../../../../apiConfig";
import { optimizeImage, removeTags } from "@/lib/functions";
import { resolutions } from "@/lib/constants";
import { useQuery } from "@tanstack/react-query";
import { SectionHeader } from "../radio/_components/SectionHeader";
import Menubutton from "@/components/general/buttons/Menubutton";
import Followbutton from "@/components/general/buttons/Followbutton";
import preImage from "../../../../../public/images/pre_image.png";

export const Promoted = ({ sectionsDetails, data }: any) => {
    const scrollRef = useRef<HTMLDivElement | null>(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (data?.length > 0) {
            setIsLoading(false);
        }
    }, [data]);

    const handleScrollLeft = () => {
        if (scrollRef.current) {
            // Scroll left smoothly by a certain amount (e.g., 100 pixels)
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft - 800,
                behavior: "smooth",
            });
        }
    };

    const handleScrollRight = () => {
        if (scrollRef.current) {
            // Scroll right smoothly by a certain amount (e.g., 100 pixels)
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft + 800,
                behavior: "smooth",
            });
        }
    };

    return (
        data?.length > 0 && (
            <section className="pt-[4rem] pb-[2rem]  relative">
                <SectionHeader
                    title={sectionsDetails.heading}
                    description={sectionsDetails.subHeading}
                    isSeeAll={sectionsDetails.showSeeAll}
                    isSliderIcons={false}
                    handleScrollLeft={handleScrollLeft}
                    handleScrollRight={handleScrollRight}
                    designName={sectionsDetails.designName}
                    position={sectionsDetails.position}
                />
                <div className="ml-[30px]" ref={scrollRef}>
                    {data?.map(
                        (item: any, index: any) =>
                            index < 1 && (
                                <div
                                    className="relative flex w-[98%] h-[200px] items-start gap-[30px] bg-[#252931] rounded-[8px]"
                                    key={index}
                                >
                                    <div className=" w-[200px] h-[200px] overflow-hidden rounded-[8px] cursor-pointer">
                                        <div className="absolute top-[5%] right-[1%] cursor-pointer">
                                            <Menubutton
                                                width={30}
                                                height={30}
                                            />
                                        </div>
                                        <div className="absolute bottom-[5%] right-[1%] cursor-pointer">
                                            <Followbutton
                                                width={30}
                                                height={30}
                                            />
                                        </div>
                                        <Image
                                            unoptimized={true}
                                            // src={
                                            //     !isLoading
                                            //         ? optimizeImage(
                                            //               item?.id,
                                            //               item?.imageUrl ||
                                            //                   item?.image,
                                            //               resolutions.sponsored
                                            //           )
                                            //         : preImage
                                            // }
                                            src={
                                                !isLoading
                                                    ? item?.imageUrl
                                                        ? item?.imageUrl
                                                        : `http://covers.netsip.co.uk/imgConvert.php/${item?.id}/${resolutions.sponsored}x${resolutions.sponsored}?url=${item?.image}`
                                                    : preImage
                                            }
                                            width={10}
                                            height={10}
                                            alt="Image"
                                            className="transition-transform duration-300 ease-in-out hover:scale-105 h-full w-full cursor-pointer"
                                            style={{
                                                borderRadius: "7px",
                                            }}
                                        />
                                    </div>
                                    <div className="mt-[40px] w-[83%]">
                                        <h1 className="text-[24px] font-semibold primary-color truncate">
                                            {item?.title}
                                        </h1>
                                        <h2 className="text-[14px] font-normal primary-color mt-[5px] line-clamp-4">
                                            {item?.itunesAuthor}
                                        </h2>
                                        <h2 className="text-[15px] font-normal text-[#9f9f9f] mt-[10px] line-clamp-4 w-[85%] h-[72px]">
                                            {removeTags(item?.description)}
                                        </h2>
                                    </div>
                                </div>
                            )
                    )}
                </div>
            </section>
        )
    );
};
