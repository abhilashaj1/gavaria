import ColorThief from "colorthief";

export const getDominantColor = async (imageUrl) => {
    const colorThief = new ColorThief();
    const image = new Image();
    image.crossOrigin = "Anonymous";

    return new Promise((resolve, reject) => {
        image.onload = () => {
            // Ensure that the image is fully loaded before attempting to extract the color
            try {
                const color = colorThief.getColor(image);
                resolve(color);
            } catch (error) {
                reject(error);
            }
        };

        image.onerror = (error) => {
            reject(error);
        };

        // Set the source of the image after attaching event listeners to ensure they trigger
        image.src = imageUrl;
    });
};
