"use client";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import { fetchData } from "../../../../../apiConfig";
import { resolutions } from "@/lib/constants";
import { optimizeImage } from "@/lib/functions";
import { useQuery } from "@tanstack/react-query";
import { SectionHeader } from "../radio/_components/SectionHeader";
import preImage from "../../../../../public/images/pre_image.png";

export const HistoryHit = ({ sectionsDetails, data }: any) => {
    const scrollRef = useRef<HTMLDivElement | null>(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (data?.length > 0) {
            setIsLoading(false);
        }
    }, [data]);

    const handleScrollLeft = () => {
        if (scrollRef.current) {
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft - 800,
                behavior: "smooth",
            });
        }
    };

    const handleScrollRight = () => {
        if (scrollRef.current) {
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft + 800,
                behavior: "smooth",
            });
        }
    };

    return (
        data?.length > 0 && (
            <section className="relative pt-[4rem] pb-[2rem]">
                <SectionHeader
                    title={sectionsDetails.heading}
                    description={sectionsDetails.subHeading}
                    position={sectionsDetails.position}
                    isSeeAll={sectionsDetails.showSeeAll}
                    handleScrollLeft={handleScrollLeft}
                    handleScrollRight={handleScrollRight}
                    designName={sectionsDetails.designName}
                />
                <div
                    className="flex items-start overflow-x-auto whitespace-nowrap card_container pl-[2rem]"
                    ref={scrollRef}
                >
                    {data?.map((item: any, index: any) => (
                        <div
                            className="relative bg-gradient-to-b via-gray-900 to-gray-900 rounded-md mr-[20px] w-60 flex-shrink-0 overflow-hidden cursor-pointer hover:border border-solid border-[2px] hover:border-white hover:rounded-[8.5px]"
                            key={index}
                        >
                            <div className="rounded-md w-full overflow-hidden">
                                <Image
                                    unoptimized={true}
                                    // src={
                                    //     !isLoading
                                    //         ? optimizeImage(
                                    //               item?.id,
                                    //               item?.imageUrl,
                                    //               resolutions.historyHit
                                    //           )
                                    //         : preImage
                                    // }
                                    src={
                                        !isLoading
                                            ? item?.imageUrl
                                                ? item?.imageUrl
                                                : `http://covers.netsip.co.uk/imgConvert.php/${item?.id}/${resolutions.historyHit}x${resolutions.historyHit}?url=${item?.image}`
                                            : preImage
                                    }
                                    width={10}
                                    height={10}
                                    alt="Image"
                                    className="transition-transform duration-300 ease-in-out transform hover:scale-105 h-full w-full"
                                    style={{
                                        borderRadius: "7px",
                                    }}
                                />
                            </div>
                            <div className="mt-[-50%] p-5 pt-[45%] relative bg-gradient-to-b from-transparent via-gray-900 to-gray-900">
                                <h2 className="text-lg font-[16px] primary-color overflow-hidden whitespace-nowrap text-ellipsis">
                                    {item?.title}
                                </h2>
                                <div className="flex items-center w-full my-1.5">
                                    <Image
                                        src={"/icons/golden-star.svg"}
                                        width={11}
                                        height={11}
                                        alt="Image"
                                        className="mr-1"
                                        loading="lazy"
                                    />

                                    <p className="text-sm font-normal text-gray-600 overflow-hidden text-ellipsis">
                                        4.5 (1.1k) | Comedy{" "}
                                    </p>
                                </div>
                                <div className="flex items-center justify-between">
                                    <div className="flex items-center bg-white rounded-md p-1.5 cursor-pointer">
                                        <div className="cursor-pointer">
                                            <Image
                                                src={"/icons/play-black.svg"}
                                                width={12}
                                                height={12}
                                                alt="Image"
                                                loading="lazy"
                                            />
                                        </div>
                                        <span className="ml-1 text-sm font-semibold text-black">
                                            Trailer
                                        </span>
                                    </div>

                                    <Image
                                        src={"/icons/round-plus.svg"}
                                        width={30}
                                        height={30}
                                        alt="Image"
                                        className="cursor-pointer"
                                        loading="lazy"
                                    />
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
            </section>
        )
    );
};
