import fetchApiData from "@/config/fetch-api-data";
import React from "react";
import { PodcastSigleRow } from "./PodcastSigleRow";
import { PodacastSingleExtra } from "./PodacastSingleExtra";
import { PodcastDobleRow } from "./PodcastDobleRow";
import { CardRectangleSmall } from "./CardRectangleSmall";
import { CardRectagleBig } from "./CardRectagleBig";
import FeaturedPodcast from "./FeaturedPodcast";
import { postData } from "@/config/fetch-post-data";
import { Promoted } from "./Promoted";
import { HistoryHit } from "./HistoryHit";
import { BrowsebyCategories } from "./BrowsebyCategories";

async function PodcastListing({ sectionsDetails, type }: any) {
    try {
        const { apiUrl } = sectionsDetails;
        const id = sectionsDetails?.item?.id;
        const method = sectionsDetails?.apiUrl?.method;

        const limit = apiUrl?.limit || "";

        const url = id
            ? `${apiUrl}${id}?limit=${limit}`
            : `${apiUrl?.url}/?limit=${limit}`;

        const data = await fetchApiData(url);
        // const postdatas = await postData(`${apiUrl?.url}`, {});

        if (!data) {
            console.error("Failed to fetch data.");
            return <div>Failed to load data.</div>;
        }

        return (
            <div>
                {type == "isdouble" ? (
                    <PodcastDobleRow
                        data={data.docs}
                        sectionsDetails={sectionsDetails}
                    />
                ) : type == "isdouble" ? (
                    // <PodcastDobleRow
                    //     data={postdatas.docs}
                    //     sectionsDetails={sectionsDetails}
                    // />
                    ""
                ) : type == "issingle" ? (
                    <PodacastSingleExtra
                        docs={data.docs}
                        datas={data.items}
                        sectionsDetails={sectionsDetails}
                    />
                ) : type == "issmall" ? (
                    <CardRectangleSmall
                        data={data.docs}
                        sectionsDetails={sectionsDetails}
                    />
                ) : type == "isbig" ? (
                    <CardRectagleBig
                        data={data.docs}
                        sectionsDetails={sectionsDetails}
                    />
                ) : type == "featured" ? (
                    <FeaturedPodcast
                        data={data.items}
                        sectionsDetails={sectionsDetails}
                    />
                ) : type == "promoted" ? (
                    <Promoted
                        data={data.items}
                        sectionsDetails={sectionsDetails}
                    />
                ) : type == "hits" ? (
                    <HistoryHit
                        data={data.items}
                        sectionsDetails={sectionsDetails}
                    />
                ) : type == "browsby" ? (
                    // <BrowsebyCategories sectionsDetails={sectionsDetails} />
                    ""
                ) : (
                    <PodcastSigleRow
                        data={data.items}
                        sectionsDetails={sectionsDetails}
                    />
                )}
            </div>
        );
    } catch (error) {
        console.error("Error fetching data:", error);
        return <div>Failed to load data.</div>;
    }
}

export default PodcastListing;
