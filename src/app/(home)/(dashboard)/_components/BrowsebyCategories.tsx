"use client";
import React, { useState, useEffect, useRef } from "react";
import Image from "next/image";
import { browseByCategoriesData } from "@/lib/data";
import { useQuery } from "@tanstack/react-query";
import Link from "next/link";
import { SectionHeader } from "../radio/_components/SectionHeader";
import { fetchData } from "../../../../../apiConfig";
import configVariables from "@/variables/configVariables";

export const BrowsebyCategories = ({ sectionsDetails }: any) => {
    const baseUrl = configVariables.baseUrl;

    const [activeLink, setActiveLink] = useState(
        browseByCategoriesData[0].slug
    );
    const [cards, setCards] = useState([]);
    const scrollRef = useRef(null);

    // const handleLinkClick = (slug) => {
    //     setActiveLink(slug);
    // };

    const fetchCardsData = async () => {
        const apiUrl = `${baseUrl}${sectionsDetails.apiUrl.url}`;
        let accessToken =
            "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdF9uYW1lIjoiQWRtaW4iLCJsYXN0X25hbWUiOiJVc2VyIiwiZW1haWwiOiJhZG1pbkB5b3BtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIn0sImlhdCI6MTY5MDIwMDI2OCwiZXhwIjoxNzIxNzU3ODY4fQ.vPiZifNO34I0YQMOcvBi-A3hw6jHDJ3B8cuEPgUpvCM";

        const response = await fetch(apiUrl, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${accessToken}`,
            },
            body: JSON.stringify({
                type: "podcast",
                keyword: "classical",
                // limit: sectionsDetails.apiUrl.limit,
                page: null,
            }),
        });

        if (!response.ok) {
            throw new Error("Failed to fetch data");
        }

        return response.json();
    };

    useEffect(() => {
        const getData = async () => {
            try {
                const data = await fetchCardsData();
                setCards(data.docs);
            } catch (error) {
                console.error("Error fetching data: ", error);
            }
        };

        getData();
    }, []);

    // const handleScrollLeft = () => {
    //     if (scrollRef.current) {
    //         scrollRef.current.scrollTo({
    //             left: scrollRef.current.scrollLeft - 800,
    //             behavior: "smooth",
    //         });
    //     }
    // };

    // const handleScrollRight = () => {
    //     if (scrollRef.current) {
    //         scrollRef.current.scrollTo({
    //             left: scrollRef.current.scrollLeft + 800,
    //             behavior: "smooth",
    //         });
    //     }
    // };

    const chunkArray = (arr: any, size: any) => {
        const chunkedArr = [];
        for (let i = 0; i < arr.length; i += size) {
            chunkedArr.push(arr.slice(i, i + size));
        }
        return chunkedArr;
    };

    const cardChunks = chunkArray(cards || [], 4);

    return (
        <section className="relative mt-12">
            <SectionHeader
                title={sectionsDetails.heading}
                description={sectionsDetails.subHeading}
                isSeeAll={sectionsDetails.showSeeAll}
                designName={sectionsDetails.designName}
                position={sectionsDetails.position}
                requrid
            />

            <div
                className="flex flex-nowrap overflow-x-auto ml-5"
                ref={scrollRef}
            >
                {cardChunks.map((chunk, columnIndex) => (
                    <div className="flex flex-col mr-5" key={columnIndex}>
                        {chunk.map((item: any, index: any) => (
                            <Link
                                href={`/podcast/${item.name}/?name=${item.name}`}
                                className="mb-5"
                                key={index}
                            >
                                <div
                                    className={`flex items-center mb-6 cursor-pointer px-4 py-2 ${
                                        activeLink === item.slug
                                            ? "bg-pink-200 rounded-lg"
                                            : ""
                                    }`}
                                    // onClick={() => handleLinkClick(item.slug)}
                                >
                                    <div className="flex items-center space-x-2">
                                        <Image
                                            unoptimized={true}
                                            src={item?.category_image}
                                            width={18}
                                            height={18}
                                            alt="Image"
                                        />
                                        <span className="text-white text-sm">
                                            {item?.name}
                                        </span>
                                    </div>
                                </div>
                            </Link>
                        ))}
                    </div>
                ))}
            </div>
        </section>
    );
};
