"use client";
import React, { useEffect, useRef, useState } from "react";
import Image from "next/image";
import { optimizeImage, removeTags } from "@/lib/functions";
import { resolutions } from "@/lib/constants";
import { getDominantColor } from "./colorUtils";
import preImage from "../../../../../public/images/pre_image.png";
import { SectionHeader } from "../radio/_components/SectionHeader";

export const FeaturedPodcast = ({ sectionsDetails, data }: any) => {
    const scrollRef = useRef<HTMLDivElement | null>(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (data?.length > 0) {
            setIsLoading(false);
        }
    }, [data]);

    const handleScrollLeft = () => {
        if (scrollRef.current) {
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft - 800,
                behavior: "smooth",
            });
        }
    };

    const handleScrollRight = () => {
        if (scrollRef.current) {
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft + 800,
                behavior: "smooth",
            });
        }
    };

    return (
        data?.length > 0 && (
            <section className="pt-[4rem] relative mt-[-15px] ">
                <SectionHeader
                    title={sectionsDetails.heading}
                    description={sectionsDetails.subHeading}
                    isSeeAll={sectionsDetails.showSeeAll}
                    handleScrollLeft={handleScrollLeft}
                    handleScrollRight={handleScrollRight}
                    designName={sectionsDetails.designName}
                    position={sectionsDetails.position}
                />

                <div
                    className="flex items-start overflow-x-scroll card_container whitespace-nowrap mb-10 pl-[2rem] py-1 "
                    ref={scrollRef}
                >
                    {data?.map((item: any, index: any) => (
                        <FeaturedCard
                            key={index}
                            cardData={item}
                            isLoading={isLoading}
                        />
                    ))}
                </div>
            </section>
        )
    );
};

const FeaturedCard = ({ cardData, isLoading }: any) => {
    const [dominantColor, setDominantColor] = useState("rgb(5, 5, 5)");

    useEffect(() => {
        if (!cardData.imageUrl) {
            console.error("Image URL not provided for card data:", cardData);
            return;
        }

        getDominantColor(cardData.imageUrl)
            .then((color: any) => {
                if (color && color.length === 3) {
                    const [R, G, B] = color;
                    setDominantColor(`rgb(${R}, ${G}, ${B})`);
                } else {
                    console.error(
                        "Invalid color format received for card data:",
                        cardData
                    );
                }
            })
            .catch((error: any) => {
                console.error(
                    "Error getting dominant color for card data:",
                    cardData,
                    "Error:",
                    error
                );
            });
    }, [cardData.imageUrl]);

    return (
        <div
            className="relative p-10 rounded-lg mr-[20px] flex-shrink-0 w-[365px] mb-2.5 cursor-pointer transition-outline duration-300 outline outline-2 outline-transparent hover:outline-white"
            style={{
                background: `linear-gradient(77deg, rgba(31, 30, 30, 0.729) 0%, ${dominantColor} 98%)`,
            }}
        >
            <div className="absolute top-[2%] right-[3%] cursor-pointer">
                <Image
                    src={"/icons/card-menu.svg"}
                    width={30}
                    height={30}
                    alt="Menu Icon"
                    loading="lazy"
                />
            </div>
            <div className="absolute bottom-[2%] right-[3%] cursor-pointer">
                <Image
                    src={"/icons/round-plus.svg"}
                    width={30}
                    height={30}
                    alt="Plus Icon"
                    loading="lazy"
                />
            </div>
            <div className="rounded-lg overflow-hidden h-[300px] w-[300px]">
                <Image
                    unoptimized={true}
                    // src={
                    //     !isLoading
                    //         ? optimizeImage(
                    //               cardData?.id,
                    //               cardData?.imageUrl,
                    //               resolutions.featured
                    //           )
                    //         : preImage
                    // }
                    src={
                        !isLoading
                            ? cardData?.imageUrl
                                ? cardData?.imageUrl
                                : `http://covers.netsip.co.uk/imgConvert.php/${cardData?.id}/${resolutions.featured}x${resolutions.featured}?url=${cardData?.image}`
                            : preImage
                    }
                    width={300}
                    height={300}
                    alt="Podcast Image"
                    className="transition-transform duration-300 ease-in-out hover:scale-105"
                />
            </div>
            <div className="mt-1.5">
                <h1 className="text-lg font-semibold primary-color mb-2 overflow-hidden whitespace-nowrap text-ellipsis">
                    {cardData?.title}
                </h1>
                <h2 className="text-base font-normal primary-color overflow-hidden whitespace-nowrap text-ellipsis mb-10">
                    {removeTags(cardData?.description)}
                </h2>
            </div>
        </div>
    );
};

export default FeaturedPodcast;
