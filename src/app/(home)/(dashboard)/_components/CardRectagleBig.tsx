"use client";
import React, { useEffect, useState, useRef } from "react";
import Image from "next/image";
import { optimizeImage, removeTags } from "@/lib/functions";
import { resolutions } from "@/lib/constants";
import preImage from "../../../../../public/images/pre_image.png";
import PlayButton from "@/components/general/buttons/PlayButton";
import Menubutton from "@/components/general/buttons/Menubutton";
import Followbutton from "@/components/general/buttons/Followbutton";
import SkeletonLoader from "@/components/general/SkeletonLoader";
import { SectionHeader } from "../radio/_components/SectionHeader";

export const CardRectagleBig = ({ sectionsDetails, data }: any) => {
    // Add isLoading prop
    const scrollRef = useRef<HTMLDivElement | null>(null);
    const [isLoading, setIsLoading] = useState(true);

    useEffect(() => {
        if (data?.length > 0) {
            setIsLoading(false);
        }
    }, [data]);

    const handleScrollLeft = () => {
        if (scrollRef.current) {
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft - 800,
                behavior: "smooth",
            });
        }
    };

    const handleScrollRight = () => {
        if (scrollRef.current) {
            scrollRef.current.scrollTo({
                left: scrollRef.current.scrollLeft + 800,
                behavior: "smooth",
            });
        }
    };

    const chunkArray = (arr: any, size: any) => {
        const chunkedArr = [];
        for (let i = 0; i < arr.length; i += size) {
            chunkedArr.push(arr.slice(i, i + size));
        }
        return chunkedArr;
    };

    const cardChunks = chunkArray(data || [], 1);

    return (
        <section className="relative mt-[4rem]">
            <SectionHeader
                title={sectionsDetails.heading}
                description={sectionsDetails.subHeading}
                isSeeAll={sectionsDetails.showSeeAll}
                handleScrollLeft={handleScrollLeft}
                handleScrollRight={handleScrollRight}
                position={sectionsDetails.position}
                designName={sectionsDetails.designName}
            />
            <div
                className="card_container flex flex-nowrap overflow-x-auto pl-[30px] scrollbar-hide"
                ref={scrollRef}
            >
                {!data
                    ? // Render skeleton loaders while data is loading
                      Array.from({ length: 10 }).map((_, index) => (
                          <SkeletonLoader key={index} />
                      ))
                    : cardChunks.map((chunk, columnIndex) => (
                          <div
                              className="column flex flex-col mr-[20px]  w-[340px]"
                              key={columnIndex}
                          >
                              {chunk.map((item: any, index: any) => (
                                  <div
                                      className="card rounded-[8px]  w-[340px] pt-[3px] flex-none"
                                      key={index}
                                  >
                                      <div className="relative cursor-pointer overflow-hidden rounded-md transition duration-300 ease-in-out h-[217px] pod_image">
                                          <div
                                              className="icon absolute right-[5%] cursor-pointer z-10"
                                              style={{ top: "5%" }}
                                          >
                                              <Menubutton
                                                  width={30}
                                                  height={30}
                                              />
                                          </div>
                                          <div
                                              className="icon absolute right-[5%] cursor-pointer z-10"
                                              style={{ bottom: "3%" }}
                                          >
                                              <Followbutton
                                                  width={30}
                                                  height={30}
                                              />
                                          </div>

                                          <div
                                              style={{
                                                  height: "217px",
                                                  width: "340px",
                                              }}
                                              className="pod_image"
                                          >
                                              <Image
                                                  unoptimized={true}
                                                  src={
                                                      !isLoading
                                                          ? item?.imageUrl
                                                              ? item?.imageUrl
                                                              : `http://covers.netsip.co.uk/imgConvert.php/${item?.id}/${resolutions.featured}x${resolutions.featured}?url=${item?.image}`
                                                          : preImage
                                                  }
                                                  width={10}
                                                  height={10}
                                                  alt="Image"
                                                  className="[transition:0.3s_ease] h-full w-full min-h-[80%] overflow-hidden hover:scale-105 hover:[transition:0.3s_ease]"
                                                  style={{
                                                      borderRadius: "7px",
                                                  }}
                                              />
                                          </div>
                                      </div>
                                      <div className="mb-[20px] mt-[6px]">
                                          <h1 className="primary-color mt-[2px] overflow-hidden whitespace-nowrap overflow-ellipsis text-[18px] font-medium">
                                              {item?.title}
                                          </h1>

                                          <h2 className="secondry-color overflow-hidden whitespace-nowrap overflow-ellipsis text-[14px] font-normal">
                                              {removeTags(
                                                  item?.itunesOwnerName
                                              )}
                                          </h2>
                                      </div>
                                  </div>
                              ))}
                          </div>
                      ))}
            </div>
        </section>
    );
};
