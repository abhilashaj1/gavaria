"use client";
import React from "react";

function error() {
    return (
        <div className="h-[100vh] w-full flex justify-center items-center color-[#fff] text-[#fff] text-[35px]">
            Server Error
        </div>
    );
}

export default error;
