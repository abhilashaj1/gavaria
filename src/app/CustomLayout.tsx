"use client";
import { Footer } from "@/components/general/Footer";
import SomeOtherComponent from "@/components/general/spotlight/SomeOtherComponent";

import { usePathname } from "next/navigation";

const CustomLayout = ({ children }: any) => {
    const pathname = usePathname();
    const includeComponentPaths = ["/", "/music", "/radio", "/audiobooks"];
    const shouldIncludeComponent = includeComponentPaths.includes(pathname);

    return (
        <div>
            {shouldIncludeComponent && (
                <>
                    <SomeOtherComponent />
                </>
            )}
            <div className="min-h-[80vh]">{children}</div>
            <Footer />
        </div>
    );
};

export default CustomLayout;
