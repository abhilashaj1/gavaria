import React from "react";

function Loading() {
    return (
        <div
            style={{
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
                minHeight: "100dvh",
            }}
        >
            <div className="text-center text-[16px] flex justify-center items-center min-h-[300px]">
                <div className="table-loader"></div>
            </div>
        </div>
    );
}
export default Loading;
