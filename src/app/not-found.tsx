export default function Custom404() {
    return (
        <div className="h-[100vh] w-full flex justify-center items-center color-[#fff] text-[#fff] text-[35px]">
            Page not found
        </div>
    );
}
