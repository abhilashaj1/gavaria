"use client";
import React, { useState, useEffect, useRef } from "react";
import Image from "next/image";
import Link from "next/link";
import { usePathname } from "next/navigation";

const Navbar = ({ navRef, sticky }: any) => {
    const [isOpen, setIsOpen] = useState(false);
    const [activeSection, setActiveSection] = useState("/podcast");
    if (activeSection === "/") {
        setActiveSection("/podcast");
    }

    const menuRef = useRef<HTMLDivElement>(null);

    const pathname = usePathname();

    useEffect(() => {
        const handleResize = () => {
            if (window.innerWidth > 590) {
                setIsOpen(true);
            } else {
                setIsOpen(false);
            }
        };

        handleResize();
        window.addEventListener("resize", handleResize);

        const handleClickOutside = (event: MouseEvent) => {
            if (
                menuRef.current &&
                !menuRef.current.contains(event.target as Node)
            ) {
                setIsOpen(false);
            }
        };

        window.addEventListener("click", handleClickOutside);

        return () => {
            window.removeEventListener("resize", handleResize);
            window.removeEventListener("click", handleClickOutside);
        };
    }, []);

    const toggleMenu = () => {
        if (window.innerWidth <= 590) {
            setIsOpen(!isOpen);
        }
    };

    const handleSectionChange = (sectionName: any) => {
        setActiveSection(sectionName);
        setIsOpen(false);
    };

    return (
        <nav
            className={`w-full py-4 bg-[#191d24] h-20 z-30 ${
                sticky ? "fixed top-0 z-1000" : "absolute bottom-0 h-34"
            }`}
            ref={navRef}
        >
            <div className="mx-8 flex items-center justify-between">
                <div className="flex items-center">
                    {sticky && (
                        <Link
                            href="/"
                            className="transition-all duration-300 ease-in-out w-[100px]"
                            scroll={true}
                        >
                            <Image
                                src="/images/gavaria-logo-white.png"
                                width={100}
                                height={100}
                                alt="Gavaria"
                                style={{
                                    opacity: sticky ? 1 : 0,
                                    width: sticky ? "100px" : "0",
                                }}
                            />
                        </Link>
                    )}
                    {(isOpen || window.innerWidth > 590) && (
                        <div
                            className={`flex items-center  transition-transform duration-300 ease-in-out ${
                                sticky
                                    ? "translate-x-5 ml-[2rem]"
                                    : "-translate-x-32 ml-[8rem]"
                            }`}
                        >
                            <Link
                                href="/"
                                className={`text-lg primary-color font-light py-2 px-5 mr-5 rounded transition-all duration-300 ease-in-out hover:bg-[#f8355833] ${
                                    pathname === "/" ? "bg-[#f8355833]" : ""
                                }`}
                                onClick={() => handleSectionChange("/")}
                                scroll={true}
                            >
                                Podcast
                            </Link>
                            <Link
                                href="/music"
                                className={`text-lg primary-color font-light py-2 px-5 mr-5 rounded transition-all duration-300 ease-in-out hover:bg-[#f8355833] ${
                                    pathname === "/music"
                                        ? "bg-[#f8355833]"
                                        : ""
                                }`}
                                onClick={() => handleSectionChange("/music")}
                                scroll={true}
                            >
                                Music
                            </Link>
                            <Link
                                href="/radio"
                                className={`text-lg primary-color font-light py-2 px-5 mr-5 rounded transition-all duration-300 ease-in-out hover:bg-[#f8355833] ${
                                    pathname === "/radio"
                                        ? "bg-[#f8355833]"
                                        : ""
                                }`}
                                onClick={() => handleSectionChange("/radio")}
                                scroll={true}
                            >
                                Radio
                            </Link>
                            <Link
                                href="/audiobooks"
                                className={`text-lg primary-color font-light py-2 px-5 mr-5 rounded transition-all duration-300 ease-in-out hover:bg-[#f8355833] ${
                                    pathname === "/audiobooks"
                                        ? "bg-[#f8355833]"
                                        : ""
                                }`}
                                onClick={() =>
                                    handleSectionChange("/audiobooks")
                                }
                                scroll={true}
                            >
                                Audiobooks
                            </Link>
                            <Link
                                href="/news"
                                className={`text-lg primary-color font-light py-2 px-5 mr-5 rounded transition-all duration-300 ease-in-out hover:bg-[#f8355833] ${
                                    pathname === "/news" ? "bg-[#f8355833]" : ""
                                }`}
                                onClick={() => handleSectionChange("/news")}
                                scroll={true}
                            >
                                News
                            </Link>
                        </div>
                    )}
                </div>
                <div className="flex items-center justify-end w-3/5">
                    <div className="flex items-center justify-between bg-[#252931] rounded px-4 py-2 w-1/2 hover:shadow-md transition-shadow duration-200">
                        <input
                            type="text"
                            className="bg-transparent w-4/5 text-lg primary-color capitalize focus:outline-none"
                            placeholder={`Search ${activeSection}`}
                        />
                        <div className="w-6 h-6">
                            <Image
                                width={26}
                                height={26}
                                alt="Search"
                                src={"/icons/search.svg"}
                            />
                        </div>
                    </div>
                    <div
                        className="ml-3 cursor-pointer block md:hidden"
                        onClick={toggleMenu}
                        ref={menuRef}
                    >
                        <Image
                            width={40}
                            height={40}
                            alt="Menu"
                            src={"/icons/menu.svg"}
                        />
                    </div>
                </div>
            </div>
        </nav>
    );
};
export default Navbar;
