import React from "react";

const SkeletonLoader = () => {
    return (
        <div className="card rounded-[8px] mb-[10px] w-[250px] pb-[80px] flex-none mr-[20px] h-[250px]">
            <div className="relative overflow-hidden rounded-md transition duration-300 ease-in-out h-[250px] skeleton-loader " />
            <div className="mb-[20px] mt-[6px]">
                <div className="skeleton-text primary-color mt-[2px] text-[18px] font-medium"></div>
                <div className="skeleton-text secondry-color text-[14px] font-normal"></div>
            </div>
        </div>
    );
};

export default SkeletonLoader;
