import Image from "next/image";
import React from "react";

function Menubutton({ width, height }: any) {
    return (
        <Image
            src="/icons/card-menu.svg"
            width={width}
            height={height}
            alt="Menu Icon"
            loading="lazy"
            className="menuicon"
        />
    );
}

export default Menubutton;
