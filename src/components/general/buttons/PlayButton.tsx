import Image from "next/image";
import React from "react";

function PlayButton({ width, height }: any) {
    return (
        <Image
            src="/icons/play-round.svg"
            width={width}
            height={height}
            alt="Plus Icon"
            className="plusicon"
            loading="lazy"
        />
    );
}

export default PlayButton;
