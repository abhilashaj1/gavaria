import Image from "next/image";
import React from "react";

function Followbutton({ width, height }: any) {
    return (
        <Image
            src={"/icons/round-plus.svg"}
            width={width}
            height={height}
            alt="Image"
            loading="lazy"
        />
    );
}

export default Followbutton;
