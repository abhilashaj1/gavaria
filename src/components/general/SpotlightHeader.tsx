import Image from "next/image";
import Link from "next/link";

const SpotlightHeader = () => {
    return (
        <nav className="py-5 px-7 sm:px-1 relative">
            <div className="flex items-center justify-between wrapper-main">
                <Link href="/">
                    <div className="w-[17%]">
                        <Image
                            src="/images/gavaria-logo-white.png"
                            width={1000}
                            height={1000}
                            alt="Gavaria"
                            loading="lazy"
                        />
                    </div>
                </Link>
                <div className="flex items-center space-x-6 xs:space-x-3 xs:-mt-5">
                    <Link
                        href="/marketplace"
                        className="primary-color text-[18px] "
                    >
                        Marketplace
                    </Link>
                    <Link
                        href="/sign-in"
                        className="border border-white border-solid border-1 rounded-[8px]  primary-color py-[12px] px-[25px] text-[18px] font-normal  "
                    >
                        Sign in
                    </Link>
                </div>
            </div>
        </nav>
    );
};

export default SpotlightHeader;
