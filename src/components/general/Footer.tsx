import React from "react";
import Image from "next/image";
import Link from "next/link";

const data = [
    { id: 1, title: "Support" },
    { id: 2, title: "Gavaria Fans" },
    { id: 3, title: "Services" },
];

export const Footer = () => {
    return (
        <footer
            className="bg-cover bg-no-repeat pt-[5rem] pb-[2rem] mt-20"
            style={{
                backgroundImage: 'url("/background-images/footer-bg.png")',
            }}
        >
            <div className="mx-24 ">
                <section className="flex flex-col items-center text-center w-[100%]">
                    <div className="flex flex-wrap justify-between items-start  w-[100%] mb-[60px]">
                        <div className="w-[19%]  mb-5">
                            <div className="w-[100%]">
                                <Image
                                    width={1000}
                                    height={1000}
                                    src="/images/gavaria-logo-footer.svg"
                                    alt="Image"
                                />
                            </div>
                        </div>
                        {data.map((item, index) => (
                            <div
                                className="text-white text-lg  text-left text-[20px] w-[15%]"
                                key={index}
                            >
                                <h1 className="mb-[16px] text-[22px]">
                                    {item?.title}
                                </h1>
                                <div className="flex flex-col  font-normal">
                                    <Link
                                        href="/"
                                        className="hover:underline mb-[4px] text-[14px] font-normal  text-[#fff] "
                                    >
                                        Health & Fitness
                                    </Link>
                                    <Link
                                        href="/"
                                        className="hover:underline mb-[4px] text-[14px] font-normal text-[#fff] "
                                    >
                                        Science
                                    </Link>
                                    <Link
                                        href="/"
                                        className="hover:underline mb-[4px] text-[14px] font-normal text-[#fff] "
                                    >
                                        Food
                                    </Link>
                                    <Link
                                        href="/"
                                        className="hover:underline mb-[4px] text-[14px] font-normal text-[#fff] "
                                    >
                                        Design
                                    </Link>
                                    <Link
                                        href="/"
                                        className="hover:underline mb-[4px] text-[14px] font-normal text-[#fff] "
                                    >
                                        Performing Arts
                                    </Link>
                                    <Link
                                        href="/"
                                        className="hover:underline  text-[14px] text-[400] text-[#fff]"
                                    >
                                        Visual Arts
                                    </Link>
                                </div>
                            </div>
                        ))}
                    </div>
                    <div className="flex justify-center items-center space-x-8 my-6">
                        <div className="w-10 cursor-pointer">
                            <Image
                                width={40}
                                height={40}
                                src="/icons/facebook.svg"
                                alt="Facebook"
                            />
                        </div>
                        <div className="w-10 cursor-pointer">
                            <Image
                                width={40}
                                height={40}
                                src="/icons/x.svg"
                                alt="X"
                            />
                        </div>
                        <div className="w-10 cursor-pointer">
                            <Image
                                width={40}
                                height={40}
                                src="/icons/youtube.svg"
                                alt="Youtube"
                            />
                        </div>
                    </div>
                    <div className="text-white text-center mx-24 lg:mx-10 sm:mx-3 mb-5">
                        <p className="text-base leading-6 mb-5">
                            All trademarks, service marks, registered
                            trademarks, registered service marks, product names,
                            product images, product screenshots, company names
                            or logos are used for product
                            identification/reference purposes only and remain
                            the exclusive property of their respective owners.
                        </p>
                        <span className="text-sm">
                            Copy Right © Gavaria 2023 | All Rights Reserved
                        </span>
                    </div>
                </section>
            </div>
        </footer>
    );
};
