"use client";
import { useEffect, useState } from "react";
import useSWR from "swr";
import Spotlight from "./Spotlight";

interface Section {
    designName: string;
    // Define other properties of Section as needed
}

interface SectionsResponse {
    section: Section[];
}

const fetcher = async (url: string): Promise<SectionsResponse> => {
    const response = await fetch(url, {
        cache: "force-cache",
    });
    const data = await response.json();
    return data;
};

export default function HeaderComponents() {
    const [isMobile, setIsMobile] = useState(false);

    const baseUrl = "https://woodenclouds.in/demo/gavaria/web/podcast";
    const {
        data: sections,
        error: isError,
        isLoading,
    } = useSWR<SectionsResponse>(baseUrl, fetcher, {
        dedupingInterval: 3600000,
        refreshInterval: 3600000,
        revalidateOnFocus: false,
        revalidateOnReconnect: false,
    });

    useEffect(() => {
        const handleResize = () => {
            setIsMobile(window.innerWidth < 280);
        };

        handleResize(); // Call it initially

        window.addEventListener("resize", handleResize);
        return () => window.removeEventListener("resize", handleResize);
    }, []);

    const renderSections = () => {
        return sections?.section?.map((section, index) => {
            switch (section.designName) {
                case "PromotedSlideSection":
                    return <Spotlight key={index} />;
                default:
                    return null;
            }
        });
    };

    return <div>{renderSections()}</div>;
}
