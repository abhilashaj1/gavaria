"use client";
import React, { useEffect, useState, useRef } from "react";
import styles from "./spotlight.module.css";
import Image from "next/image";
import { fetchData } from "../../../../apiConfig";
import { getDominantColor, optimizeImage, removeTags } from "@/lib/functions";
import { resolutions } from "@/lib/constants";
import preImage from "../../../../public/images/pre_image.png";
import Link from "next/link";
import Navbar from "../Navbar";
import SpotlightHeader from "../SpotlightHeader";

const Spotlight = () => {
    const accessToken =
        "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1c2VyIjp7ImlkIjoxLCJmaXJzdF9uYW1lIjoiQWRtaW4iLCJsYXN0X25hbWUiOiJVc2VyIiwiZW1haWwiOiJhZG1pbkB5b3BtYWlsLmNvbSIsInJvbGUiOiJ1c2VyIn0sImlhdCI6MTY5MDIwMDI2OCwiZXhwIjoxNzIxNzU3ODY4fQ.vPiZifNO34I0YQMOcvBi-A3hw6jHDJ3B8cuEPgUpvCM";
    const baseUrl = "http://apidev.gavaria.com/api/";
    const path = "collection/items/6420041ad57b4dadda75573d";
    const url = `${baseUrl}${path}`;
    const [spotlightData, setSpotlightData] = useState([]);
    const [error, setError] = useState(null);
    const [isLoading, setIsLoading] = useState(true);
    const [listDats, setListDats] = useState([]);
    const [currentItem, setCurrentItem] = useState(0);
    const [dominantColor, setDominantColor] = useState(
        "rgba(5, 5, 5, 0.5676864495798319)"
    );
    const [bgImage, setBgImage] = useState(null);
    console.log(bgImage, "bgImage");
    const [currentItemId, setCurrentItemId] = useState("");
    console.log(currentItemId, "currentItemId");
    const [slidePosition, setSlidePosition] = useState(1450);
    const [sticky, setSticky] = useState(false);

    const spotlightRef = useRef();
    const navRef = useRef();
    const wrapperContainerRef = useRef();
    const autoScrollIntervalRef = useRef(null);

    useEffect(() => {
        const fetchData = async () => {
            setIsLoading(true);
            try {
                const response = await fetch(url, {
                    headers: {
                        Authorization: `Bearer ${accessToken}`,
                    },
                });
                if (!response.ok) {
                    throw new Error("Network response was not ok");
                }
                const data = await response.json();

                if (data && data.items) {
                    setSpotlightData(data?.items);
                    setListDats(data?.items);
                } else {
                }
            } catch (error) {
                setError(error);
                console.error("Fetch error:", error);
            } finally {
                setIsLoading(false);
            }
        };

        fetchData();

        const intervalId = setInterval(fetchData, 3600000);

        return () => clearInterval(intervalId);
    }, [url]);

    const handleNext = (index) => {
        setCurrentItem(index);
        setBgImage(spotlightData[index]?.imageUrl);
        setCurrentItemId(spotlightData[index]?.id);
        const items = document.querySelectorAll(".slider-item");
        items.forEach((slide, i) => {
            slide.style.transform = `translateX(-${index * slidePosition}px)`;
        });
    };

    const handlePrev = () => {
        let prevIndex = currentItem - 1;
        if (prevIndex < 0) {
            prevIndex = spotlightData.length - 1;
        }
        handleNext(prevIndex);
    };

    const handleResize = () => {
        if (window?.innerWidth > 2600) {
            setSlidePosition(2600);
        } else if (window?.innerWidth > 1900) {
            setSlidePosition(1900);
        } else if (window?.innerWidth > 1800) {
            setSlidePosition(1800);
        } else if (window?.innerWidth > 1600) {
            setSlidePosition(1600);
        }
    };

    const handleScroll = () => {
        const spotlight = spotlightRef.current;
        const nav = navRef.current;

        const spotlightTop = spotlight?.clientHeight;
        const scrollY = window.scrollY;

        if (nav) {
            if (scrollY + 80 > spotlightTop) {
                setSticky(true);
            } else {
                setSticky(false);
            }
        }
    };

    const stopAutoScroll = () => {
        clearInterval(autoScrollIntervalRef.current);
    };

    const startAutoScroll = () => {
        autoScrollIntervalRef.current = setInterval(() => {
            const nextIndex = (currentItem + 1) % spotlightData?.length;
            handleNext(nextIndex);
        }, 12000);
    };

    useEffect(() => {
        {
            bgImage &&
                spotlightData &&
                getDominantColor(bgImage, 8)
                    .then(({ R, G, B }) => {
                        setDominantColor(`rgb(${R}, ${G}, ${B})`);
                    })
                    .catch((error) => {
                        console.error("Error getting average color:", error);
                    });
        }
    }, [bgImage, spotlightData]);

    useEffect(() => {
        handleResize();

        window.addEventListener("resize", handleResize);
        window.addEventListener("scroll", handleScroll);

        return () => {
            window.removeEventListener("resize", handleResize);
            window.removeEventListener("scroll", handleScroll);
        };
    }, []);

    useEffect(() => {
        if (listDats?.length > 0) {
            let datas;
            datas = listDats[0];
            setBgImage(spotlightData[0]?.imageUrl);
            setCurrentItemId(spotlightData[0]?.id);
        }
    }, [spotlightData]);

    useEffect(() => {
        startAutoScroll();

        return () => {
            stopAutoScroll();
        };
    }, [currentItem, spotlightData]);

    return (
        <section
            className={styles.main}
            ref={spotlightRef}
            style={{
                background: `linear-gradient(77deg, rgba(31, 30, 30, 0.729) 0%, ${
                    dominantColor || `rgb(5, 5, 5)`
                } 68%)`,
            }}
            // onMouseEnter={stopAutoScroll}
            // onMouseLeave={startAutoScroll}
        >
            <div className={styles.main_overlay}>
                <div className={styles.wrapper_bg}>
                    <img
                        className="image"
                        width={20}
                        height={20}
                        // src={
                        //     isLoading
                        //         ? preImage
                        //         : optimizeImage(
                        //               currentItemId,
                        //               bgImage,
                        //               resolutions.spotlight
                        //           )
                        // }
                        src={
                            !isLoading
                                ? bgImage
                                    ? bgImage
                                    : `http://covers.netsip.co.uk/imgConvert.php/${currentItemId}/${resolutions.spotlight}x${resolutions.spotlight}`
                                : preImage
                        }
                        alt="Image"
                        loading="lazy"
                    />
                </div>
            </div>
            <SpotlightHeader />

            <div className={styles.slide_container} ref={wrapperContainerRef}>
                <div className={styles.top}>
                    {spotlightData?.map(
                        (item, index) =>
                            index < 8 && (
                                <section
                                    key={index}
                                    className={`${styles.wrapper} slider-item`}
                                    style={{
                                        left: `${index * slidePosition}px`,
                                    }}
                                >
                                    <div className={styles.left}>
                                        <Link href={"/marketplace"}>
                                            {" "}
                                            <h1 className={styles.title}>
                                                {item?.title}
                                            </h1>
                                        </Link>
                                        <h2 className={styles.author}>
                                            {item?.itunesOwnerName}
                                        </h2>
                                        <div className={styles.categories}>
                                            {item?.category1 &&
                                            item?.category2 &&
                                            item?.category3 ? (
                                                <>
                                                    <Link
                                                        href={`podcast/${item.slug}/?name=${item.category1}`}
                                                        className={
                                                            styles.category
                                                        }
                                                    >
                                                        {item.category1}
                                                    </Link>
                                                    <span
                                                        className={styles.dot}
                                                    ></span>
                                                    <Link
                                                        href={`podcast/${item.slug}/?name=${item.category2}`}
                                                        className={
                                                            styles.category
                                                        }
                                                    >
                                                        {item.category2}
                                                    </Link>
                                                    <span
                                                        className={styles.dot}
                                                    ></span>
                                                    <Link
                                                        href={`podcast/${item.slug}/?name=${item.category3}`}
                                                        className={
                                                            styles.category
                                                        }
                                                    >
                                                        {item.category3}
                                                    </Link>
                                                </>
                                            ) : (
                                                <Link
                                                    href={`podcast/${item.slug}/?name=${item?.category1}`}
                                                    className={styles.category}
                                                >
                                                    {item?.category1 ||
                                                        item?.category2 ||
                                                        item?.category3}
                                                </Link>
                                            )}
                                        </div>
                                        <p className={styles.descrption}>
                                            {removeTags(item?.description)}
                                        </p>
                                        <div className={styles.button_box}>
                                            <div className={styles.play}>
                                                <span>Follow Me</span>
                                                <Image
                                                    width={18}
                                                    height={18}
                                                    src={
                                                        "/icons/plus_color.svg"
                                                    }
                                                    alt="Play Icon"
                                                    style={{
                                                        borderRadius: "50%",
                                                    }}
                                                />
                                            </div>
                                            <div className={styles.icon}></div>
                                        </div>
                                    </div>
                                    <div className={styles.right}>
                                        <Link href={"/marketplace"}>
                                            <div className={styles.pod_image}>
                                                <Image
                                                    width={30}
                                                    height={30}
                                                    // src={
                                                    //     !isLoading
                                                    //         ? optimizeImage(
                                                    //               item?.id,
                                                    //               item.imageUrl,
                                                    //               resolutions.spotlight
                                                    //           )
                                                    //         : preImage
                                                    // }
                                                    src={
                                                        !isLoading
                                                            ? item.imageUrl
                                                                ? item.imageUrl
                                                                : `http://covers.netsip.co.uk/imgConvert.php/${item?.id}/${resolutions.spotlight}x${resolutions.spotlight}?url=${item?.image}`
                                                            : preImage
                                                    }
                                                    className="image"
                                                    alt="Podcast Image"
                                                    loading="lazy"
                                                    unoptimized={true}
                                                />
                                            </div>
                                        </Link>
                                    </div>
                                </section>
                            )
                    )}
                </div>
                <div className={styles.bottom}>
                    <div className={styles.slider_dots}>
                        {spotlightData?.map(
                            (item, index) =>
                                index < 8 && (
                                    <span
                                        onClick={() => handleNext(index)}
                                        key={index}
                                        className={styles.slider_dot}
                                        style={{
                                            backgroundColor:
                                                currentItem === index
                                                    ? "#b4b4b4"
                                                    : "#b4b4b499",
                                        }}
                                    ></span>
                                )
                        )}
                    </div>
                </div>
                <div className={styles.curvy_bg}>
                    <Image
                        width={30}
                        height={30}
                        src={"/background-images/spotlight-curvy-bg.svg"}
                        className="image"
                        alt="Curvy Background"
                        loading="lazy"
                    />
                </div>
            </div>
            <Navbar navRef={navRef} sticky={sticky} />
        </section>
    );
};

export default Spotlight;
