"use client";
import React from "react";
import { QueryClient, QueryClientProvider } from "@tanstack/react-query";
import HederComponets from "./HederComponets";

const SomeOtherComponent = () => {
    return (
        <>
            <QueryClientProvider client={queryClient}>
                <HederComponets />
            </QueryClientProvider>
        </>
    );
};

export default SomeOtherComponent;

const queryClient = new QueryClient();
